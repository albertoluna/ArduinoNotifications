package arduinonotifications.luna.dev.arduinonotifications;

import android.app.Notification;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by lual on 13/10/16.
 */

public class NotificationListener extends NotificationListenerService {
    BluetoothConnection connection;
    ObjectMapper mapper;

    public NotificationListener() {
        super();
        connection = BluetoothConnection.getInstance();
        mapper = new ObjectMapper();
    }

    public void onNotificationPosted(StatusBarNotification sbn) {
        super.onNotificationPosted(sbn);
        final PackageManager pm = getApplicationContext().getPackageManager();
        ApplicationInfo ai;
        try {
            ai = pm.getApplicationInfo(sbn.getPackageName(), 0);
        } catch (final PackageManager.NameNotFoundException e) {
            ai = null;
        }
        NotificationDTO notification = new NotificationDTO();
        final String applicationName = (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");
        notification.setAppName(applicationName);
        notification.setAppPackage(sbn.getPackageName());
        notification.setContent(sbn.getNotification().extras.getString(Notification.EXTRA_TEXT));
        notification.setTitle(sbn.getNotification().extras.getString(Notification.EXTRA_TITLE));

        if (notification.getContent() == null) {
            return;
        }

        NotificationAction notificationAction = new NotificationAction();
        notification.setTitle(sbn.getNotification().extras.getString(Notification.EXTRA_TITLE));
        notificationAction.setText(sbn.getNotification().extras.getString(Notification.EXTRA_TEXT));
        notificationAction.setAppName(applicationName);
        notificationAction.setAppPackage(sbn.getPackageName());
        notificationAction.setAction(NotificationActionType.ADD);
        notificationAction.setColor(new int[]{0, 100, 0});

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String mac = prefs.getString("mac", null);
        if (mac == null) {
            return;
        }

        try {
            if (connection.getMac() == null) {
                connection.initialice(mac);
            }

            String jsonData = mapper.writeValueAsString(notificationAction);
            connection.send(jsonData);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    public void onNotificationRemoved(StatusBarNotification sbn) {
        super.onNotificationRemoved(sbn);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
