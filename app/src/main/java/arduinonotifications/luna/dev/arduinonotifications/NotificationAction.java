package arduinonotifications.luna.dev.arduinonotifications;

/**
 * Created by lual on 16/10/16.
 */

public class NotificationAction {
    private String appName;
    private String appPackage;
    private String title;
    private String text;
    private int[] color;
    private NotificationActionType action;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppPackage() {
        return appPackage;
    }

    public void setAppPackage(String appPackage) {
        this.appPackage = appPackage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public NotificationActionType getAction() {
        return action;
    }

    public void setAction(NotificationActionType action) {
        this.action = action;
    }

    public int[] getColor() {
        return color;
    }

    public void setColor(int[] color) {
        this.color = color;
    }
}
