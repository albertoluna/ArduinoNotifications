package arduinonotifications.luna.dev.arduinonotifications;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by lual on 16/10/16.
 */

public class BluetoothConnection {
    private BluetoothSocket socket;
    private BluetoothDevice device;
    private String mac;
    private static BluetoothConnection instance;

    public static BluetoothConnection getInstance() {
        if (instance == null) {
            instance = new BluetoothConnection();
        }
        return instance;
    }

    private BluetoothConnection() {
    }

    public void initialice(String mac) {
        this.mac = mac;
        try {
            device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(mac);
            socket = (BluetoothSocket) device.getClass().getMethod("createRfcommSocket", new Class[] {int.class}).invoke(device,1);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public void connect() {
        try {
            socket.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void send(String data) {
        if (!socket.isConnected()) {
            this.connect();
        }
        try {
            socket.getOutputStream().write(data.getBytes());
            socket.getOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public BluetoothSocket getSocket() {
        return socket;
    }

    public void setSocket(BluetoothSocket socket) {
        this.socket = socket;
    }

    public BluetoothDevice getDevice() {
        return device;
    }

    public void setDevice(BluetoothDevice device) {
        this.device = device;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
}
