package arduinonotifications.luna.dev.arduinonotifications;

import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ListActivity {
    ArrayAdapter<String> adapter;
    List<String> adapterValues = new ArrayList<>();
    List<String> macs = new ArrayList<>();
    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        setContentView(R.layout.activity_main);
        LocalBroadcastManager.getInstance(this).registerReceiver(onNotification, new IntentFilter("Notification"));
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, adapterValues);
        setListAdapter(adapter);

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(enableBtIntent);
        }

        // Register the BroadcastReceiver
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy
        mBluetoothAdapter.startDiscovery();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // unregisterReceiver(onNotification);
        unregisterReceiver(mReceiver);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        String mac = macs.get(position);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("mac", mac);
        editor.apply();
    }

    private BroadcastReceiver onNotification = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            NotificationDTO notification = new NotificationDTO();
            notification.setAppPackage(intent.getStringExtra("appPackage"));
            notification.setAppPackage(intent.getStringExtra("appName"));
            notification.setTitle(intent.getStringExtra("title"));
            notification.setContent(intent.getStringExtra("content"));

            adapterValues.add(notification.getAppName());
            adapter.notifyDataSetChanged();
        }
    };

    // Create a BroadcastReceiver for ACTION_FOUND
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Add the name and address to an array adapter to show in a ListView
                device.createBond();
                macs.add(device.getAddress());
                adapter.add(device.getName() + "\n" + device.getAddress());
            }
        }
    };
}
