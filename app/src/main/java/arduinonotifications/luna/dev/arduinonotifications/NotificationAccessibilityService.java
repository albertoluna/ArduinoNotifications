package arduinonotifications.luna.dev.arduinonotifications;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Notification;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.view.accessibility.AccessibilityEvent;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by lual on 13/10/16.
 */

public class NotificationAccessibilityService extends AccessibilityService {
    BluetoothSocket socket;
    ObjectMapper mapper;

    protected void onServiceConnected() {
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.eventTypes = AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED;
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_ALL_MASK;
        info.notificationTimeout = 100;
        setServiceInfo(info);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent e) {
        if (e.getEventType() == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED) {
            Parcelable data = e.getParcelableData();
            if (data instanceof Notification) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String mac = prefs.getString("mac", null);
                if (mac == null) {
                    return;
                }

                Notification notification = (Notification) data;
                final PackageManager pm = getApplicationContext().getPackageManager();
                ApplicationInfo ai;
                try {
                    ai = pm.getApplicationInfo(e.getPackageName().toString(), 0);
                } catch (final PackageManager.NameNotFoundException ex) {
                    ai = null;
                }
                NotificationDTO notificationDTO = new NotificationDTO();
                final String applicationName = (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");
                notificationDTO.setAppName(applicationName);
                notificationDTO.setAppPackage(e.getPackageName().toString());
                notificationDTO.setContent(notification.extras.getString(Notification.EXTRA_TEXT));
                notificationDTO.setTitle(notification.extras.getString(Notification.EXTRA_TITLE));

                NotificationAction notificationAction = new NotificationAction();
                notificationAction.setTitle(notificationDTO.getTitle());
                notificationAction.setText(notificationDTO.getContent());
                notificationAction.setAppName(applicationName);
                notificationAction.setAppPackage(notificationDTO.getAppPackage());
                notificationAction.setAction(NotificationActionType.ADD);
                notificationAction.setColor(new int[]{0, 100, 0});

                BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(mac);
                mapper = new ObjectMapper();
                try {
                    if (socket == null || !socket.isConnected()) {
                        socket = (BluetoothSocket) device.getClass().getMethod("createRfcommSocket", new Class[] {int.class}).invoke(device,1);
                        socket.connect();
                    }
                    String jsonData = mapper.writeValueAsString(notificationAction);
                    socket.getOutputStream().write(jsonData.getBytes());
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onInterrupt() {
        // TODO Auto-generated method stub
    }
}
